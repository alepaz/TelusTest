package com.telus.donation.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.telus.donation.entity.Country;
import com.telus.donation.repository.CountryRepository;

@Service("countryService")
public class CountryServiceImpl implements CountryService{

	@Autowired
	private CountryRepository countryRepository;
	
	@Override
	public List<Country> findCountryById(int user_id) {
		return (List<Country>) countryRepository.findById(user_id);
	}

	@Override
	public Country getCountryById(int id) {
		return countryRepository.findOne(id);
	}

	@Override
	public List<Country> getAllCountries() {
		return countryRepository.findAll();
	}

}
