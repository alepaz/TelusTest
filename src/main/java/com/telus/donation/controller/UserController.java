package com.telus.donation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telus.donation.entity.User;
import com.telus.donation.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<User> getEmployees() {
		return userService.getAllUsers();
	}

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User getDonation(@PathVariable("id") int id) {
		return userService.getUserById(id);
	}

}
