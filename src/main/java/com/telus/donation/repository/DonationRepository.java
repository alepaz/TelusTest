package com.telus.donation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telus.donation.entity.Donation;

@Repository("donationRepository")
public interface DonationRepository extends JpaRepository<Donation, Integer> {
	 List<Donation> findById(int user_id);
}
