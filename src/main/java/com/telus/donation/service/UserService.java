package com.telus.donation.service;

import java.util.List;

import com.telus.donation.entity.User;

public interface UserService {
	public User findUserByEmail(String email);
	public void saveUser(User user);
	User getUserById(int id);
	List<User> getAllUsers();
}
