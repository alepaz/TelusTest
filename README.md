# telus-online-donation-site

Developer test with the aim of evaluate knowledge and technical skills in web development areas


## Requirements
- Maven will do everything, just import in eclipse as existing maven project
- MySQL server (Phpmyadmin as for example)
![Phpmyadmin](https://github.com/alepaz/TelusTest/blob/master/README/phpmyadmin.PNG)

Database Scripts(https://github.com/alepaz/TelusTest/tree/master/Database)

## Getting started

- Clone this repo

`$ git clone git@github.com:alepaz/TelusTest.git`

Change DB Conexion
https://github.com/alepaz/TelusTest/blob/master/src/main/resources/application.properties

## Deliverables

- Entity Relationship diagram.
![Database diagram](https://github.com/alepaz/TelusTest/blob/master/README/Diagrama-ER.PNG)

- Webservice.
![Example Webservice](https://github.com/alepaz/TelusTest/blob/master/README/webservice.PNG)

## RestFul Paths
- /donation/{id}
- /donation/
- /country/{id}
- /country/
- /institution/{id}
- /institution/
- /user/{id}
- /user/
