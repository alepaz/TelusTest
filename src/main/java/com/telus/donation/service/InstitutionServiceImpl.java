package com.telus.donation.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.telus.donation.entity.Institution;
import com.telus.donation.repository.InstitutionRepository;

@Service("institutionService")
public class InstitutionServiceImpl implements InstitutionService{

	@Autowired
	private InstitutionRepository institutionRepository;
	
	@Override
	public List<Institution> findInstitutionById(int user_id) {
		return (List<Institution>) institutionRepository.findById(user_id);
	}

	@Override
	public Institution getInstitutionById(int id) {
		return institutionRepository.findOne(id);
	}

	@Override
	public List<Institution> getAllInstitutions() {
		return institutionRepository.findAll();
	}

}
