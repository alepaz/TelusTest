package com.telus.donation.controller;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telus.donation.entity.Institution;
import com.telus.donation.service.InstitutionService;

@RestController
public class InstitutionController {
	@Autowired
	private InstitutionService institutionService;
	
    @RequestMapping(value = "/institution", method = RequestMethod.GET)
    public List<Institution> getInstitutions() {
		return institutionService.getAllInstitutions();
	}

    @RequestMapping(value = "/institution/{id}", method = RequestMethod.GET)
    public Institution getInstitution(@PathVariable("id") int id) {
		return institutionService.getInstitutionById(id);
	}

}
