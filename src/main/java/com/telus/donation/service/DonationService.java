package com.telus.donation.service;

import java.util.List;

import com.telus.donation.entity.Donation;

public interface DonationService {
	public Donation getDonationById(int id);
	public List<Donation> getAllDonations();
	public List<Donation> findDonationById(int user_id);
	public void saveDonation(Donation donation);

}
