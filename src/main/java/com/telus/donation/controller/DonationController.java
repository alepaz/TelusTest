package com.telus.donation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telus.donation.entity.Donation;
import com.telus.donation.service.DonationService;

@RestController
public class DonationController {
	
	@Autowired
	private DonationService donationService;
	
    @RequestMapping(value = "/donation", method = RequestMethod.GET)
    public List<Donation> getEmployees() {
		return donationService.getAllDonations();
	}

    @RequestMapping(value = "/donation/{id}", method = RequestMethod.GET)
    public Donation getDonation(@PathVariable("id") int id) {
		return donationService.getDonationById(id);
	}

}
