package com.telus.donation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telus.donation.entity.Country;
import com.telus.donation.service.CountryService;

@RestController
public class CountryController {
	
	@Autowired
	private CountryService countryService;
	
    @RequestMapping(value = "/country", method = RequestMethod.GET)
    public List<Country> getEmployees() {
		return countryService.getAllCountries();
	}

    @RequestMapping(value = "/country/{id}", method = RequestMethod.GET)
    public Country getCountry(@PathVariable("id") int id) {
		return countryService.getCountryById(id);
	}

}
