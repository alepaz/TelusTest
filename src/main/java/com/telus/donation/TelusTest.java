package com.telus.donation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelusTest {

	public static void main(String[] args) {
		SpringApplication.run(TelusTest.class, args);
	}
}
