package com.telus.donation.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.telus.donation.entity.User;
import com.telus.donation.entity.Donation;
import com.telus.donation.entity.Institution;
import com.telus.donation.service.UserService;
import com.telus.donation.service.DonationService;
import com.telus.donation.service.InstitutionService;

@Controller
public class LoginController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private DonationService donationService;
	@Autowired
	private InstitutionService institutionService;	

	@RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
	public ModelAndView login(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/registration", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"El correo ya se encuentra registrado");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "Usuario registrado correctamente");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/donation", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView createDonation(@RequestParam("institution") int institution,
			@RequestParam("donation") Double donation,
			@RequestParam("creditNumber") String creditNumber,
			@RequestParam("mesCaducidad") int mesCaducidad,
			@RequestParam("anioCaducidad") int anioCaducidad,
			@RequestParam("nameInCard") String nameInCard) {
		ModelAndView modelAndView = new ModelAndView();
		
		if(String.valueOf(creditNumber).length() == 16) {
			Donation donationObject = new Donation();
			donationObject.setAmount(donation);
			//donationObject.setDate_donation(LocalDate.now());
			donationObject.setInstitution_id(institution);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findUserByEmail(auth.getName());
			donationObject.setUser_id(user.getId());
			modelAndView.addObject("successMessage", "Donación Realizada");
			
			donationService.saveDonation(donationObject);
			
		}else {
			modelAndView.addObject("successMessage", "Tarjeta incorrecta");
			modelAndView.setViewName("admin/donation");
		}


		/*User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult
					.rejectValue("email", "error.user",
							"El correo ya se encuentra registrado");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "Usuario registrado correctamente");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("registration");
			
		}*/
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/home", method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("userName", "Bienvenido " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
		modelAndView.addObject("adminMessage","Contenido disponible para usuarios registrados");
		modelAndView.setViewName("admin/home");
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/donation", method = RequestMethod.GET)
	public ModelAndView donation(){
		ModelAndView modelAndView = new ModelAndView();
		Donation donation = new Donation();
		modelAndView.addObject("donation", donation);
		modelAndView.setViewName("admin/donation");
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/historial", method = RequestMethod.GET)
	public ModelAndView historial(){
		ModelAndView modelAndView = new ModelAndView();
		List<Donation> donation = new ArrayList<Donation>();
		List<Institution> institution = new ArrayList<Institution>();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		//Code who needs to be refactor
		donation = donationService.getAllDonations();
		institution = institutionService.getAllInstitutions();
		
		try {
			donation = donation.stream().filter(p -> p.getUser_id() == user.getId()).collect(Collectors.toList());
		}catch (Exception e) {
			System.out.println("Error");
		}
		
		//System.out.println(donation);
		modelAndView.addObject("donation", donation);
		modelAndView.addObject("institution", institution);		
		modelAndView.setViewName("admin/historial");
		return modelAndView;
	}
	

}
