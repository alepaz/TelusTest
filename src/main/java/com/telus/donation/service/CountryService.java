package com.telus.donation.service;

import java.util.List;

import com.telus.donation.entity.Country;

public interface CountryService {
	public Country getCountryById(int id);
	public List<Country> getAllCountries();
	public List<Country> findCountryById(int user_id);

}
