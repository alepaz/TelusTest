package com.telus.donation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telus.donation.entity.Institution;

@Repository("institutionRepository")
public interface InstitutionRepository extends JpaRepository<Institution, Integer> {
	List<Institution> findById(int user_id);
}
