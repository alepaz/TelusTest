package com.telus.donation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telus.donation.entity.Country;

@Repository("countryRepository")
public interface CountryRepository extends JpaRepository<Country, Integer> {
	 List<Country> findById(int id);
}
