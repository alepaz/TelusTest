package com.telus.donation.service;

import java.util.List;

import com.telus.donation.entity.Institution;

public interface InstitutionService {
	public Institution getInstitutionById(int id);
	public List<Institution> getAllInstitutions();
	public List<Institution> findInstitutionById(int user_id);

}
