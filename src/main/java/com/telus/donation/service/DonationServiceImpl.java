package com.telus.donation.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.telus.donation.entity.Donation;
import com.telus.donation.repository.DonationRepository;

@Service("donationService")
public class DonationServiceImpl implements DonationService{

	@Autowired
	private DonationRepository donationRepository;
	
	@Override
	public List<Donation> findDonationById(int user_id) {
		return (List<Donation>) donationRepository.findById(user_id);
	}

	@Override
	public void saveDonation(Donation donation) {
		donationRepository.save(donation);
	}

	@Override
	public Donation getDonationById(int id) {
		return donationRepository.findOne(id);
	}

	@Override
	public List<Donation> getAllDonations() {
		return donationRepository.findAll();
	}

}
